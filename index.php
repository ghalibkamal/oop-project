<?php

require('animal.php');
require('frog.php');
require('ape.php');


$sheep = new Animal("shaun");

echo $sheep->name();
echo "<br>";
echo $sheep->legs();
echo "<br>";
echo $sheep->cold_blooded();
echo "<br> <br>"; 



$kodok = new frog("buduk");
echo $kodok->jump(); // "hop hop"
echo "<br> <br>";

$sungokong = new Ape("kera sakti");
$sungokong->yell() // "Auooo"

?>