<?php

    class Animal {
        public $name;
        public $legs =2;
        public $cold_blooded = 'false';

        function __construct($nama){
            $this->name = $nama;
        }
        function name(){
            return $this->name;
        }
        function legs(){
            return $this->legs;
        }
        function cold_blooded(){
            return $this->cold_blooded;
        }
    
    }

?>